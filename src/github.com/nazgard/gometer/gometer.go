package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"time"
	"strings"
	"net"
	"github.com/dustin/go-humanize"
)

type Pocket struct {
	rx uint64
	tx uint64
}

type Speed struct {
	rx uint64
	tx uint64
}

type Interface struct {
	Name string
	Pockets Pocket
	Speed Speed
}

func (i Interface) String() string {
	speed := i.getSpeed(1 * time.Second)
	s := "Интерфейс "+i.Name + "\t\t"
	s += "[RX: "+ humanize.Bytes(speed.rx) + "/s] [TX: " + humanize.Bytes(speed.tx)+"/s]"
	return s
}

func (i Interface) getSpeed(duration time.Duration) Speed {
	prevInterfaceBytes := i.getBytes()

	time.Sleep(duration)

	b := i.getBytes()
	s := Speed{rx:b.rx - prevInterfaceBytes.rx, tx:b.tx - prevInterfaceBytes.tx}

	return s
}

func (i Interface) getBytes() (data Pocket) {
	data = Pocket{rx:0, tx:0}
	rxBytes, _ := ioutil.ReadFile("/sys/class/net/" + i.Name + "/statistics/rx_bytes")
	txBytes, _ := ioutil.ReadFile("/sys/class/net/" + i.Name + "/statistics/tx_bytes")

	rxInt, _ := strconv.Atoi(strings.Trim(string(rxBytes), "\n"))
	txInt, _ := strconv.Atoi(strings.Trim(string(txBytes), "\n"))

	data.rx = uint64(rxInt)
	data.tx = uint64(txInt)
	return data
}

func main() {
	interfaces, _ := net.Interfaces()
	for {
		s := ""
		for _, value := range interfaces {
			t := Interface{Name:value.Name}
			t.Pockets = t.getBytes()
			s += t.String() + "\n"
		}
		fmt.Print(s)
		fmt.Println()
	}
}
